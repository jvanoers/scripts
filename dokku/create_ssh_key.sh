#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 [KEY_NAME] [DESCRIPTION]"
    echo "e.g. $0 id_rsa_jenkins jenkins"
    exit 1
fi

DOKKU_STORAGE_DIR=/var/lib/dokku/data/storage
DOKKU_KEY_DIR=$DOKKU_STORAGE_DIR/keys

if [ ! -d "$DOKKU_STORAGE_DIR" ]; then
    echo "Cannot find storage directory at: $DOKKU_STORAGE_DIR !"
    echo "Please ensure you are running on the dokku host."
    exit 1
fi

if [ ! -d "$DOKKU_KEY_DIR" ]; then
    mkdir -p $DOKKU_KEY_DIR
fi

echo "Creating key with name $1..."
ssh-keygen -t rsa -b 4096 -C "$2" -f "$DOKKU_KEY_DIR/$1"

echo "Key created: $DOKKU_KEY_DIR/$1"
