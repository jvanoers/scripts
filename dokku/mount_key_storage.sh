#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 [APP_NAME]"
    echo "e.g. $0 jenkins"
    exit 1
fi

DOKKU_STORAGE_DIR=/var/lib/dokku/data/storage
DOKKU_KEY_DIR=$DOKKU_STORAGE_DIR/keys

if [ ! -d "$DOKKU_KEY_DIR" ]; then
    echo "WARN: Cannot find storage directory at: $DOKKU_KEY_DIR !"
    echo "Creating $DOKKU_KEY_DIR..."
    if [ ! -d "$DOKKU_KEY_DIR" ]; then
        mkdir -p $DOKKU_KEY_DIR
    fi
fi

echo "Mounting storage..."
dokku storage:mount $1 $DOKKU_KEY_DIR:/keys/