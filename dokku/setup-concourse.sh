#!/bin/bash

deploy_docker_image() {
    DOCKER_IMAGE=$1
    VERSION=$2
    DOKKU_APPNAME=$3

    dokku apps:create $DOKKU_APPNAME

    docker pull $DOCKER_IMAGE:$VERSION

    docker tag $DOCKER_IMAGE:$VERSION dokku/$DOKKU_APPNAME:$VERSION

    dokku tags:deploy $DOKKU_APPNAME $VERSION
}

# DB
deploy_docker_image postgres 9.5 concourse-db
dokku config:set concourse-db \
        POSTGRES_DB=concourse \
        POSTGRES_USER=concourse \
        POSTGRES_PASSWORD=changeme \
        PGDATA=\/database

# Web
deploy_docker_image concourse\/concourse latest concourse-web

dokku storage:mount concourse-web "./keys/web:/concourse-keys"
dokku config:set concourse-web \
        CONCOURSE_BASIC_AUTH_USERNAME=concourse \
        CONCOURSE_BASIC_AUTH_PASSWORD=changeme \
        CONCOURSE_EXTERNAL_URL="concourse-web.dokku.home" \
        CONCOURSE_POSTGRES_DATA_SOURCE=postgres://concourse:changeme@concourse-db.dokku.home:5432/concourse?sslmode=disable

# Worker
deploy_docker_image concourse\/concourse latest concourse-worker
dokku docker-options:add concourse-worker deploy "--privileged"
dokku storage:mount concourse-worker "./keys/worker:/concourse-keys"

dokku config:set concourse-worker \
        CONCOURSE_TSA_HOST=concourse-web.dokku.home